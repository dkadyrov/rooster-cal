from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import TemplateView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # path('', TemplateView.as_view(template_name='home.html'), name='home'),
    path('', include('calendarapp.urls')),    
    path('admin/', admin.site.urls),
    path('users/', include('users.urls')),
    # path('users/', include('django.contrib.auth.urls')),
    path('account/', include('allauth.urls')),
    ]
urlpatterns += staticfiles_urlpatterns() 
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
