# users/admin.py
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import User, Calendar



class UserAdmin(UserAdmin):
    model = User
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    # list_display = ['email']
    # fieldsets = (
    #     ('User Information', {
    #         'fields': ('email',),
    #     }),
    # )
admin.site.register(User, UserAdmin)
admin.site.register(Calendar)