from django.contrib.auth.models import AbstractUser, UserManager
from calendarapp.models import Host, Event
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

class UserManager(UserManager):
    pass


class User(AbstractUser):
    objects = UserManager()

    def __str__(self):
        return self.email

class Calendar(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    hosts = models.ManyToManyField(Host, blank=True)
    events = models.ManyToManyField(Event, blank=True)

@receiver(post_save, sender=User)
def create_calendar(sender, created, instance, **kwargs):
    if created:
        calendar = Calendar(user=instance)
        calendar.save()

# post_save.connect(create_calendar, sender=User)
