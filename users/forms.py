from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import User

from allauth.account.forms import SignupForm
from django import forms


class CustomUserCreationForm(SignupForm):
    class Meta(SignupForm):
        model = User
        fields = ('username', 'email', )
class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = User
        fields = UserChangeForm.Meta.fields
