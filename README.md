# Rooster Cal

## Installation and Running

Download the Roostser Cal Package from Git: 

- Ideally Git Clone it onto your personal computer so you can push changes
  
- Read up on Git protocal (pretty much progression is "git pull" whenever there are changes you did not download, to make a change, then run "git commit -m 'message about the changes you made', and then run "git push".)

Download Anaconda for Python 3.7: https://www.anaconda.com/distribution/

- Makes things easier for developing on Mac (which has Python preinstalled)
  
- I'm developing in Conda and my packages are mixed between Conda and Pip (Python installer)

Once you have Anaconda installed, create the environment from the YML file: 

To Run the Server:

```bash

```