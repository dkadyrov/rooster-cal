from django.urls import path, include, re_path
from django.conf.urls import url
from rest_framework import routers
from django.contrib import admin, auth
from . import views
from calendarapp.feeds.ics_feed import HostFeed, EventFeed, iCalEventFeed, iCalHostFeed, iCalCalendarFeed, CalendarFeed

urlpatterns = [
    path('', views.home, name='home'),
    path('collections', views.collections, name='collections'),
    path('users/feeds/<username>.ics', iCalCalendarFeed(), name='calendarics'),
    path('users/feeds/<username>', CalendarFeed(), name='calendarrss'),
    # path('hosts/', views.hosts.as_view(), name='hosts'),
    path('discover/', views.discover, name='discover'),
    path('hosts/<name>/', views.host, name='host'),
    path('hosts/<name>/feed/rss', HostFeed(), name='hostrss'),
    path('hosts/<name>/feed/<subname>.ics', iCalHostFeed(), name='hostics'),
    path('hosts/<name>/events/<int:event_id>/', views.event, name="event"),
    path('hosts/<name>/events/<int:event_id>.ics', iCalEventFeed(), name="eventics"),
    path('hosts/<name>/events/<int:event_id>/rss', EventFeed(), name="eventrss"),
    # path('host_to_cal/<host_id>/', views.host_to_cal, name='host_to_cal'),
    # path('event_to_cal/<event_id>/', views.event_to_cal, name='event_to_cal'),
    path('search/', views.search, name="search")
    # url(r'^search/$', views.search, name='search'), 
]
