from django.contrib.syndication.views import Feed, FeedDoesNotExist
from django.urls import reverse

from django.shortcuts import get_object_or_404
from calendarapp.models import Event, Host, Collection
from users.models import User, Calendar

from django_ical.views import ICalFeed

class HostFeed(Feed):
    title = "XML Title #TODO"
    link = "/feeds/"
    description = "XML Description #TODO"

    def get_object(self, request, name):
        return get_object_or_404(Host, name=name)

    def items(self, obj):
        return Event.objects.filter(host=obj.id)

    def title(self, obj):
        return obj.name

    def item_link(self, item):
        return 'http://127.0.0.1:8000/hosts/{}/events/{}/'.format(item.host, item.id)


class iCalHostFeed(ICalFeed):
    """
    A simple event calender
    """
    product_id = '-//example.com//Example//EN'
    timezone = 'UTC'
    file_name = "event.ics"

    def get_object(self, request, name, subname):
        return get_object_or_404(Host, name=name)

    def items(self, obj):
        return Event.objects.filter(host=obj.id)

    def title(self, obj):
        return obj.name

    def item_title(self, item):
        return item.name

    def item_description(self, item):
        return item.description

    def item_start_datetime(self, item):
        return item.start

    def item_end_datetime(self, item):
        return item.end

    def item_link(self, item):
        return 'http://127.0.0.1:8000/hosts/{}/events/{}/'.format(item.host, item.id)

    def file_name(self, obj):
        return ("%s.ics" % obj.name)


class iCalEventFeed(ICalFeed):
    """
    A simple event calender
    """
    product_id = '-//example.com//Example//EN'
    timezone = 'UTC'
    # file_name = "event.ics"

    def get_object(self, request, name, event_id):
        return get_object_or_404(Event, id=event_id)

    def items(self, obj):
        return Event.objects.filter(id=obj.id)

    def title(self, obj):
        return obj.name

    def item_title(self, item):
        return item.name

    def item_description(self, item):
        return item.description

    def item_start_datetime(self, item):
        return item.start

    def item_end_datetime(self, item):
        return item.end

    def item_link(self, item):
        return 'http://127.0.0.1:8000/hosts/{}/events/{}/'.format(item.host, item.id)
        # return reverse('event', args=[item.pk])

    def file_name(self, obj):
        return ("%s.ics" % obj.name)


class EventFeed(Feed):
    title = "XML Title #TODO"
    link = "/feeds/"
    description = "XML Description #TODO"

    def get_object(self, request, name, event_id):
        return get_object_or_404(Event, id=event_id)

    def items(self, obj):
        return Event.objects.filter(host=obj.id)

    def title(self, obj):
        return obj.name

    def item_start_datetime(self, item):
        return item.start

    def item_end_datetime(self, item):
        return item.end

    def item_link(self, item):
        return 'http://127.0.0.1:8000/hosts/{}/events/{}/'.format(item.host, item.id)

class iCalCalendarFeed(ICalFeed):
    title = "XML Title #TODO"
    link = "/feeds/"
    description = "XML Description #TODO"

    def get_object(self, request, username):
        user = get_object_or_404(User, username=username)
        return get_object_or_404(Calendar, user=user)

    def items(self, obj):
        events = obj.events.all()
        for host in obj.hosts.all():
            events = events | host.event_set.all()
        print(events)
        return events

    def title(self, obj):
        return obj.user.username

    def item_title(self, item):
        return item.name

    def item_description(self, item):
        return item.description.replace(' <br>', '\\n')

    def item_start_datetime(self, item):
        return item.start

    def item_end_datetime(self, item):
        return item.end

    def item_link(self, item):
        return 'http://127.0.0.1:8000/hosts/{}/events/{}/'.format(item.host, item.id)
        # return reverse('event', args=[item.pk])

    def file_name(self, obj):
        return ("roostercal_%s.ics" % obj.user.username)


class CalendarFeed(Feed):
    title = "XML Title #TODO"
    link = "/feeds/"
    description = "XML Description #TODO"

    def get_object(self, request, username):
        user = get_object_or_404(User, username=username)
        return get_object_or_404(Calendar, user=user)

    def items(self, obj):
        events = obj.events.all()
        for host in obj.hosts.all():
            events = events | host.event_set.all()
        print(events)
        return events

    def title(self, obj):
        return obj.user.username

    def item_start_datetime(self, item):
        return item.start

    def item_end_datetime(self, item):
        return item.end

    def item_link(self, item):
        return 'http://127.0.0.1:8000/hosts/{}/events/{}/'.format(item.host, item.id)
