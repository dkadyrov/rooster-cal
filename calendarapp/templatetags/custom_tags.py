from django.utils.http import urlquote_plus
from django import template

from datetime import datetime
import pytz

register = template.Library()

@register.filter(name="convert_timezone")
def convert_timezone(datetime):
    print("test")


@register.filter(name='google_event')
def google_event(Event):
    # tfmt = '%Y%m%dT000000'
    tfmt = '%Y%m%dT%H%M%S'
    dates = '%s%s%s%s' % (Event.start.strftime(tfmt), 'Z%2F', Event.end.strftime(tfmt), 'Z')

    name = urlquote_plus(Event.name)

    s = ('http://www.google.com/calendar/event?action=TEMPLATE&' +
            'text=' + name + '&' +
            'dates=' + dates)

    if Event.location:
        s = s + '&location=' + urlquote_plus(Event.location)

    return s + '&trp=false'

# google_event.safe = True
