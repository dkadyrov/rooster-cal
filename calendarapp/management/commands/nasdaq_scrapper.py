from django.core.management.base import BaseCommand
from calendarme.models import Company, Event, Calendar

import requests
from bs4 import BeautifulSoup
import csv 
import re
from datetime import datetime, date, time
from pytz import timezone
import pytz



class Command(BaseCommand):
    help = 'Scrape Events'

    def nasdaq_scrapper(self, ticker):
        url = 'https://www.nasdaq.com/earnings/report/'
        ticker_url = url+ticker
        page = requests.get(ticker_url)
        soup = BeautifulSoup(page.content, 'html.parser')
        para = soup.find(id="two_column_main_content_reportdata")
        return para

    def _scrape_events(self):

        for c in Company.objects.raw('SELECT * FROM calendarme_company'):
            para = re.findall(r'\>(.*?)\<', str(self.nasdaq_scrapper(c.ticker)))[0]
            print(para)
            if 'before' in para:
                date = re.search(r'on (.*?) before', para).group(1)
                date = date.replace(' ', '')
                start_date = datetime.strptime(date, '%m/%d/%Y')
                end_date = start_date
                start_time = time(7, 30, tzinfo=pytz.timezone('America/New_York'))
                end_time = time(9, 30, tzinfo=pytz.timezone('America/New_York'))
                start_datetime = datetime.combine(start_date, start_time)
                end_datetime = datetime.combine(end_date, end_time)

                event_name = "%s Earnings Report".format(c.ticker)
                event_description = para

                event = Event(name=event_name, start=start_datetime, end=end_datetime, description=event_description, calendar='1')
                event.save()

            elif 'after' in para:
                date = re.search(r'on (.*?) after', para).group(1)
                date = date.replace(' ', '')
                start_date = datetime.strptime(date, '%m/%d/%Y')
                end_date = start_date
                start_time = time(4, 30, tzinfo=pytz.timezone('Americas/New York'))
                end_time = time(6, 30, tzinfo=pytz.timezone('Americas/New York'))
                start_datetime = datetime.combine(start_date, start_time)
                end_datetime = datetime.combine(end_date, end_time)

                event_name = "%s Earnings Report".format(c.ticker)
                event_description = para

                event = Event(name=event_name, start=start_datetime, end=end_datetime,
                                description=event_description, calendar='1')
                event.save()





    def handle(self, *args, **options):
        self._scrape_events()
