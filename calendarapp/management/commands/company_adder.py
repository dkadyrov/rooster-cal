from calendarapp.models import Host, Location
import requests
from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    def adder(self, host):
        # location = Location()
        ticker = host.name
        print(ticker)
        site = "https://finance.yahoo.com/quote/{}/profile?p={}".format(ticker, ticker)
        page = requests.get(site)
        soup = BeautifulSoup(page.content, 'html.parser')
        try: 
            description = soup.find_all('p', class_="Mt(15px) Lh(1.6)")
            host.description = description[0].get_text()
            contact = soup.find_all('p', class_="D(ib)")
            contact = contact[0].get_text('|')
            contact = contact.split('|')
            # print(contact)
            # location.address = contact[0]
            # location.city = contact[1].split(',')[0]
            # location.state = contact[1].split(',')[1].split(' ')[0]
            # location.zipcode = contact[1].split(',')[1].split(' ')[1]
            # location.country = contact[2]
            # location.phone = contact[3].replace('-', '')
            host.website = contact[-1]
            # location.save()
            # host.location = location
            host.save()
        except:
            pass
            
    def populate(self):
        for host in Host.objects.all():
            self.adder(host)

    def handle(self, *args, **options):
        self.populate()
