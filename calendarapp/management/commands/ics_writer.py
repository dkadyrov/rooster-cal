from datetime import datetime
import icalendar as ical
from calendarapp.models import Event, Calendar
# from icalendar import * #Calendar, Event, vDatetime, vCalAddress, vText
import pytz

def ics_writer(modelObj):
    if isinstance(modelObj, Calendar): 
        print("#TODO")

    if isinstance(modelObj, Event):
        cal = ical.Calendar()
        cal.add('prodid', '-//Rooster Calendar//{}//'.format(modelObj.name))
        cal.add('version', '2.0')
        events = event_writer(modelObj)
        cal.add_component(events)

def event_writer(eventObj):
    uuid = eventObj.start + eventObj.end + eventObj.id + '@roostercal.com'

    event = ical.Event()
    event.add('uid', uuid)
    event.add('dtstamp', datetime.now())
    event.add('summary', eventObj.name)
    event.add('dtstart', eventObj.start)
    event.add('dtend', eventObj.end)
    event.add('description', eventObj.description)
    if eventObj.location:
        event.add('location', eventObj.location)
    return event

