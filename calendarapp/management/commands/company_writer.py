from django.core.management.base import BaseCommand
from calendarapp.models import Host
import csv

class Command(BaseCommand):
    help = 'Imports Companies'

    def _add_companies(self):
        sp500 = []
        with open('calendarapp/management/commands/sp500.csv', mode='r') as infile: 
            reader = csv.reader(infile)
            next(reader)
            for rows in reader:
                sp500.append(rows[0])

        with open('calendarapp/management/commands/nasdaq.csv', mode='r') as infile:
            reader = csv.reader(infile)
            for rows in reader:
                ticker = rows[0]
                name = rows[1]
                sector = rows[6]
                industry = rows[7]
                url = rows[8]
                description = ''
                tags = ["NASDAQ", "stock", "company", "finance"]
                if ticker in sp500:
                    tags.append('sp500')
                if not Host.objects.filter(name=ticker).exists():
                    company = Host(name=ticker, subname=name, description=description)
                    company.save()
                    for tag in tags:
                        company.tags.add(tag)
                    company.save()

        with open('calendarapp/management/commands/nyse.csv', mode='r') as infile:
            reader = csv.reader(infile)
            for rows in reader:
                ticker = rows[0]
                name = rows[1]
                sector = rows[6]
                industry = rows[7]
                url = rows[8]
                description = ''
                tags = ["NYSE", "stock", "company", "finance"]
                if ticker in sp500:
                    tags.append('sp500')
                if not Host.objects.filter(name=ticker).exists():
                    company = Host(name=ticker, subname=name,
                                   description=description)
                    company.save()
                    for tag in tags:
                        company.tags.add(tag)
                    company.save()


    def handle(self, *args, **options):
        self._add_companies()
