from django.core.management.base import BaseCommand
from calendarapp.models import Host, Event

import requests
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from dateutil import parser
import pytz

class Command(BaseCommand):
    def scrapper(self, host):
        name = host.name
        print(name)
        ticker = host.name
        url = 'https://finance.yahoo.com/calendar/earnings?symbol='
        page = requests.get(url+ticker)
        soup = BeautifulSoup(page.content, 'html.parser')
        datelist = soup.find_all('td', class_="data-col2")
        expectedlist = soup.find_all('td', class_="data-col3")
        actuallist = soup.find_all('td', class_="data-col4")
        fmt = '%Y-%m-%d %H:%M:%S %Z%z'

        for i in range(len(datelist)):
            full = datelist[i].get_text()
            full = full.replace('PM', 'PM.')
            full = full.replace('AM', 'AM.')
            # tz = pytz.timezone(full.split(".")[1])
            start = parser.parse(full.split(".")[0])
            start = start + timedelta(hours=5)
            start = pytz.utc.localize(start)

            # print(start)
            # break

            # start = start.astimezone(pytz.utc)
            end = start + timedelta(hours=2)
            expected = expectedlist[i].get_text()
            actual = actuallist[i].get_text()

            title = "${} Earnings Report".format(ticker)

            description = '{} Earnings Report <br> Expected: {} <br> Actual: {}'.format(name, expected, actual)

            if not Event.objects.filter(name=title, start=start).exists():
                print(title)
                event = Event(name=title, start=start, end=end, description=description, host=host)
                event.save()
                event.tags.add('Earnings Report')
                
    def populate(self):
        for c in Host.objects.all():
            self.scrapper(c)

    def handle(self, *args, **options):
        self.populate()
