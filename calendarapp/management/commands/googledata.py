"""Example of Python client calling Knowledge Graph Search API."""
import json
# import urllib
import urllib.parse 
import urllib.request


api_key = 'AIzaSyCSXTqaccEkb5fiq61mZWfcHelQZrip-sg'  # open('.api_key').read()

class GoogleData(object):
    def __init__(self, name):
        self.elements = self.get_data(name)
        print(self.elements)
        # self.description = element['result']['detailedDescription']['articleBody']
        # self.description_url = element['result']['detailedDescription']['url']
        # self.image = element['result']['image']['contentUrl']

    def get_data(self, query):
        service_url = 'https://kgsearch.googleapis.com/v1/entities:search'
        params = {
            'query': query,
            'limit': 10,
            'indent': True,
            'key': api_key,
        }
        url = service_url + '?' + urllib.parse.urlencode(params)
        self.response = json.loads(urllib.request.urlopen(url).read())
        self.element = self.response['itemListElement']
        return element

# get_data('Tesla')
