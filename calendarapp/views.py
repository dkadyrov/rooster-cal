from django_ical.views import ICalFeed
from django.shortcuts import redirect, render
from django.shortcuts import render_to_response

from rest_framework import generics
from rest_framework import viewsets

from django.shortcuts import get_object_or_404, render
from django.views import generic
from django.template import loader
from django.http import HttpResponse, Http404, JsonResponse, HttpResponseRedirect
import json
import datetime

from django.urls import reverse_lazy

from .management.commands.googledata import GoogleData
from .models import Collection, Host, Event
from users.models import User, Calendar
from .serializers import EventSerializer, HostSerializer, CollectionSerializer

# from pure_pagination import Paginator, EmptyPage, PageNotAnInteger

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from watson import search as watson

from taggit.models import Tag

class EventView(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class HostView(viewsets.ModelViewSet):
    queryset = Host.objects.all()
    serializer_class = HostSerializer


class CollectionView(viewsets.ModelViewSet):
    queryset = Collection.objects.all()
    serializer_class = CollectionSerializer

def home(request):
    if request.user.is_authenticated:
        calendar = Calendar.objects.filter(user=request.user)[0]
        hosts = calendar.hosts.all()
        events = calendar.events.all()
        event_array = []
        # host_events = []
        # for host in hosts:
            # host_events.append(Event.objects.filter(host=host))
        # event_array = [j for i in host_events for j in i]
        for event in events:
            event_array.append(event)
        # if filters applied then get parameter and filter based on condition else return object
        if request.GET:
            event_json = []
            for i in event_array:
                event_sub_arr = {}
                event_sub_arr['title'] = i.name
                start_date = datetime.isoformat(i.start)
                end_date = datetime.isoformat(i.end)
                event_sub_arr['start'] = start_date
                event_sub_arr['end'] = end_date
                print(event_sub_arr)
                event_json.append(event_sub_arr)
            return HttpResponse(json.dumps(event_json))

        if request.POST:
            print(request.POST)
            if 'unfollowhost' in request.POST:
                host = Host.objects.get(id=request.POST.get('unfollowhost'))
                host_to_cal(host, calendar)
            if 'unfollowevent' in request.POST:
                event = Event.objects.get(id=request.POST.get('unfollowevent'))
                event_to_cal(event, calendar)
            return HttpResponseRedirect(request.path_info)

        context = {
            "event": event_array,
        }
        return render(request, 'calendarapp/home.html', context)

    else:
        return render(request, 'calendarapp/start.html')


def event(request, name, event_id):
    event = get_object_or_404(Event, id=event_id)
    # event_object = Event.objects.get(id=event_id)

    return render(request, 'calendarapp/event.html', {'event': event})


def host_to_cal(host, calendar):
    events = Event.objects.filter(host=host)
    if host in calendar.hosts.all():
        calendar.hosts.remove(host)
        for event in events: 
            calendar.events.remove(event)
    else:
        calendar.hosts.add(host)
        for event in events:
            if event in calendar.events.all():
                calendar.events.remove(event)
            calendar.events.add(event)

def event_to_cal(event, calendar):
    if event in calendar.events.all():
        calendar.events.remove(event)
    else:
        calendar.events.add(event)

def host(request, name):
    host = get_object_or_404(Host, name=name)
    events = Event.objects.filter(host=host.id)
    if request.user.is_authenticated:
        calendar = request.user.calendar
        if request.POST:
            print(request.POST)
            if 'followhost' in request.POST:
                print("follow host")
                host_to_cal(host, calendar)
            if 'followevent' in request.POST:
                print("follow event")
                event = Event.objects.get(id=request.POST.get('followevent'))
                event_to_cal(event, calendar)
            return HttpResponseRedirect(request.path_info)
    return render(request, 'calendarapp/host.html', {'host': host, 'events': events})

def discover(request):
    host_list = Host.objects.all()
    tags = Tag.objects.all()
    if request.POST:   
        print(request.POST['search'])
        if 'tags' in request.POST:
            tag = Tag.objects.get(name=request.POST.get('tags'))
            try: 
                host_list = get_object_or_404(Host, tags=tag) #Host.objects.filter(tags=tag)
            except: 
                pass
        if request.POST['search'] != '':
            query = request.POST['search']
            search_results = watson.search(query)
            host_list = []
            for search in search_results:
                if type(search.object) == Host:
                    host_list.append(search.object)
    page = request.GET.get('page', 1)

    paginator = Paginator(host_list, 12)

    try:
        hosts = paginator.page(page)
    except PageNotAnInteger:
        hosts = paginator.page(1)
    except EmptyPage:
        hosts = paginator.page(paginator.num_pages)

    return render(request, 'calendarapp/hosts.html', {'hosts': hosts, 'tags': tags})


# class hosts(generic.ListView):
#     model = Host
#     paginate_by = 12
#     # queryset = 
#     template_name = 'calendarapp/hosts.html'

#     def get_context_data(self, **kwargs):
#         context = super(hosts, self).get_context_data(**kwargs)
#         context.update({
#             # 'hosts': Host.objects.order_by('?'), 
#             'tags': Tag.objects.all(),
#         })
#         return context

#     def get_queryset(self):
#         return Host.objects.order_by('?')

def collections(request):
    collections = Collection.objects.all()
    return render(request, 'calendarapp/collections.html', {'collections': collections})


def collection(request, collection_id):
    collection = get_object_or_404(Collection, id=collection_id)
    collection_object = Group.objects.get(id=collection_id)
    hosts = Host.objects.filter(collection=collection_object)
    events = []
    for host in hosts:
        events.append(Event.objects.filter(host=host))
    events = [j for i in events for j in i]
    # events = Event.objects.filter(host=hosts)
    # info = GoogleData(host_object.subname)
    return render(request, 'calendarapp/collection.html', {'collection': collection, 'hosts': hosts, 'events': events})

def search(request):
    query = request.POST['usr_query']
    search_results = watson.search(query)
    events = []
    hosts = []
    for search in search_results:
        print(type(search.object))
        if type(search.object) == Event:
            events.append(search.object)
        if type(search.object) == Host:
            hosts.append(search.object)
    return render(request, 'search/search.html', {'hosts':hosts, 'events':events})

    
