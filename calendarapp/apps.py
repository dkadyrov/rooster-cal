from django.apps import AppConfig
from watson import search as watson

class CalendarappConfig(AppConfig):
    name = 'calendarapp'

    def ready(self):
        Event = self.get_model("Event")
        Host = self.get_model("Host")
        watson.register(Event)
        watson.register(Host)
