from django.db import models
from django.utils.http import urlquote_plus
from taggit.managers import TaggableManager
from phonenumber_field.modelfields import PhoneNumberField

class Location(models.Model):
    name = models.CharField(blank=True, null=True, max_length=200)
    address = models.CharField(blank=True, null=True, max_length=200)
    city = models.CharField(blank=True, null=True, max_length=200)
    state = models.CharField(blank=True, null=True, max_length=200)
    zipcode = models.CharField(blank=True, null=True, max_length=200)
    country = models.CharField(blank=True, null=True, max_length=200)
    phone = PhoneNumberField(blank=True, null=True)
    geo = models.CharField(blank=True, null=True, max_length=200)
    # TODO Access Google Maps Places Info
    # https://developers.google.com/places/web-service/details
    # TODO hours

class Host(models.Model):
    name = models.CharField(max_length=200)
    subname = models.CharField(max_length=200, null=True)
    location = models.ForeignKey(
        Location, null=True, blank=True, on_delete=models.SET_NULL)
    description = models.CharField(max_length=8192, blank=True, null=True)
    tags = TaggableManager()
    image = models.ImageField(blank=True, null=True)
    website = models.URLField(blank=True, null=True)
    def __str__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(max_length=200)
    start = models.DateTimeField('start datetime')
    end = models.DateTimeField('end datetime')
    description = models.CharField(max_length=8192, blank=True, null=True)
    location = models.ForeignKey(
        Location, null=True, blank=True, on_delete=models.SET_NULL)
    host = models.ForeignKey(Host, on_delete=models.CASCADE)
    tags = TaggableManager()

    def __str__(self):
        return self.name

class Collection(models.Model):
    name = models.CharField(max_length=200)
    hosts = models.ManyToManyField(Host, blank=True)
    events = models.ManyToManyField(Event, blank=True)

    def __str__(self):
        return self.name


