from calendarapp.models import Host, Event
import django_filters


class HostFilter(django_filters.FilterSet):
    class Meta:
        model = Host
        fields = ['name']