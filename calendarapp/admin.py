from django.contrib import admin

from .models import Collection, Host, Event

admin.site.register(Collection)
admin.site.register(Host)
admin.site.register(Event)